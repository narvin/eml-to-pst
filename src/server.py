"""SMTP server that writes messages to disk"""

import asyncore  # pylint: disable=deprecated-module
from datetime import datetime
from os import path
from smtpd import SMTPServer

PORT = 1025


class TestServer(SMTPServer):
    """SMTP server"""
    recv_dir = path.realpath(path.join(path.dirname(path.realpath(__file__)),
                                       "../example/recv"))

    def __init__(self, localaddr):
        super().__init__(localaddr, None)
        self.num_processed = 0

    def process_message(self, peer, mailfrom, rcpttos, data, **kwargs):
        self.num_processed += 1

        if self.num_processed % 2 == 0:
            print(f"skipped message {self.num_processed}")
            return "500 foo"

        file_path = path.join(TestServer.recv_dir,
                              f"{datetime.now():%Y.%m.%d-%H.%M.%S.%f.eml}")
        with open(file_path, "wb") as file:
            file.write(data)
        print(f"created {file_path}")


def main():
    """Run the application"""
    server = TestServer(("localhost", PORT))

    try:
        print(f"SMTP server on localhost:{PORT} starting")
        print("Press ctrl+c to stop the server")
        asyncore.loop(timeout=0.5)
    except KeyboardInterrupt:
        server.close()
        print(f"SMTP server on localhost:{PORT} closed")


if __name__ == "__main__":
    main()
