"""Send messages in EML files to an SMTP server"""

from collections import defaultdict
from configparser import ConfigParser
from datetime import datetime
from enum import Enum
from functools import reduce
import glob
import logging
from os import makedirs, path
import re
import shutil
import smtplib
import sys
from time import perf_counter, sleep

APP_NAME = "eml_to_smtp"
APP_VERSION = "0.1.0"
USAGE = """
usage: eml_to_pst [<ini_file>]

    ini_file    Path to the ini file
                Defaults to using a file in the script directory
"""


class ProcessedStatus(Enum):
    """Message processed statuses"""
    PROCESSED = "processed"
    UNPROCESSED = "unprocessed"
    SKIPPED = "skipped"


def get_args():
    """Validate and return the command line arguments"""
    if not 1 <= len(sys.argv) <= 2:
        print(USAGE)
        raise Exception("invalid number of arguments")

    _, *ini_path = sys.argv

    ini_path = path.realpath(ini_path[0]) if ini_path \
        else path.join(path.dirname(path.realpath(__file__)),
                       f"{APP_NAME}.ini")
    if not path.exists(ini_path):
        raise Exception(f"ini_path not found: {ini_path}")

    return ini_path


def create_logger():
    """Create the global app logger"""
    logging.addLevelName(logging.INFO + 1, "INFO")

    inst = logging.getLogger(__name__)
    inst.setLevel(logging.DEBUG)
    inst.info2 = lambda msg, *args, **kwargs: logger.log(logging.INFO + 1,
                                                         msg, *args, **kwargs)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(logging.Formatter(
        "%(levelname)s %(message)s"))
    console_handler.addFilter(lambda record: record.levelno != logging.INFO)
    inst.addHandler(console_handler)

    log_dir = path.join(path.dirname(sys.argv[0]), "log")
    log_basename = f"{datetime.now():%Y-%m-%d-%H-%M-%S-%f}"
    makedirs(log_dir, exist_ok=True)
    file_handler = logging.FileHandler(
        filename=path.join(log_dir, f"{log_basename[:-3]}.log"),
        encoding="utf-8",)
    file_handler.setFormatter(logging.Formatter(
        "%(asctime)s %(levelname)s %(message)s"))
    file_handler.setLevel(logging.DEBUG)
    inst.addHandler(file_handler)

    return inst


def get_config(ini_file):
    """Parse and validate the ini file"""
    def normalize_int_inf(value):
        return int(value) if not value.lower() == "inf" else float("inf")

    def normalize_list(values):
        if values is None:
            return None
        values = [value for value in values.replace("\r\n", "\n")
                                           .split("\n") if value]
        return values

    def assert_dir(dir_path):
        if not path.isdir(dir_path):
            raise NotADirectoryError(dir_path)

    def assert_val(value, name):
        if not value:
            raise ValueError(f"invalid {name}")

    def assert_opt_val(value, name):
        if value is not None and not value:
            raise ValueError(f"invalid {name} (omit if not needed)")

    def assert_in_rg(value, name, min_val=float("-inf"),
                     max_val=float("inf")):
        if not min_val <= value <= max_val:
            raise ValueError(
                f"invalid {name} (valid range is [{min_val}, {max_val}])")

    config = ConfigParser()
    config.read(ini_file)
    normalized_config = {
        "server": {
            "smtp_host": config["server"]["host"],
            "smtp_port": int(config["server"]["port"]),
            "from_addr": config["server"]["from"],
            "to_addr": config["server"]["to"],
            "max_msgs": normalize_int_inf(config["server"].get("max_msgs",
                                          "inf")),
            "max_time": float(config["server"].get("max_time", "inf")),
            "backoff_interval": float(config["server"].get("backoff_interval",
                                      "inf")),
            "backoff_duration": float(config["server"].get("backoff_duration",
                                      1.0)),
        },
        "eml": {
            "eml_dir": path.realpath(config["eml"]["eml_dir"]),
            "processed_dir": path.realpath(config["eml"]["processed_dir"]),
            "unprocessed_dir": path.realpath(config["eml"]["unprocessed_dir"]),
            "skipped_dir": path.realpath(config["eml"]["skipped_dir"]),
            "skip_senders": normalize_list(config["eml"].get("skip_senders")),
            "skip_subjects":
                normalize_list(config["eml"].get("skip_subjects")),
            "mangle_domains":
                normalize_list(config["eml"].get("mangle_sender_domains")),
        },
    }

    assert_val(normalized_config["server"]["smtp_host"], "SMTP host")
    assert_in_rg(normalized_config["server"]["smtp_port"], "SMTP port",
                 1, 65535)
    assert_val(normalized_config["server"]["from_addr"], "from")
    assert_val(normalized_config["server"]["to_addr"], "to")
    assert_in_rg(normalized_config["server"]["max_msgs"], "max_msgs", 1)
    assert_in_rg(normalized_config["server"]["max_time"], "max_time", 0)
    assert_in_rg(normalized_config["server"]["backoff_interval"],
                 "backoff_interval", 0)
    assert_in_rg(normalized_config["server"]["backoff_duration"],
                 "backoff_duration", 0)
    assert_dir(normalized_config["eml"]["eml_dir"])
    assert_dir(normalized_config["eml"]["processed_dir"])
    assert_dir(normalized_config["eml"]["unprocessed_dir"])
    assert_dir(normalized_config["eml"]["skipped_dir"])
    assert_opt_val(normalized_config["eml"]["skip_senders"], "skip_senders")
    assert_opt_val(normalized_config["eml"]["skip_subjects"], "skip_subjects")
    assert_opt_val(normalized_config["eml"]["mangle_domains"],
                   "mangle_domains")

    return normalized_config


def create_msg_post_processor(processed_dir, unprocessed_dir, skipped_dir,
                              counters, **_):
    """Move EML to a processed/unprocessed directory"""
    sub_dir = f"{datetime.now():%Y%m%d%H%M%S}"
    processed_map = {
        ProcessedStatus.PROCESSED: {
            "sub_dir": path.join(processed_dir, sub_dir),
            "is_created": False,
            "logger_fn": logger.info,
        },
        ProcessedStatus.UNPROCESSED: {
            "sub_dir": path.join(unprocessed_dir, sub_dir),
            "is_created": False,
            "logger_fn": logger.error,
        },
        ProcessedStatus.SKIPPED: {
            "sub_dir": path.join(skipped_dir, sub_dir),
            "is_created": False,
            "logger_fn": logger.info,
        },
    }

    def msg_post_proc(eml_file, processed_status):
        processed = processed_map[processed_status]
        sub_dir = processed["sub_dir"]

        if not processed["is_created"]:
            if path.exists(sub_dir):
                raise RuntimeError(f"{sub_dir} already exists")
            logger.info("creating %s dir: %s", processed_status.value, sub_dir)
            makedirs(sub_dir)
            processed["is_created"] = True

        processed["logger_fn"]("moving %s to %s dir", path.basename(eml_file),
                               processed_status.value)
        counters[processed_status.value] += 1
        shutil.move(eml_file, sub_dir)

    return msg_post_proc


def create_msg_skip_processor(skip_senders, skip_subjects, msg_post_proc,
                              counters):
    """Skip EML for certain senders"""
    if not skip_senders and not skip_subjects:
        def noop_fn(*_):
            pass
        return noop_fn

    def create_process(re_obj, template, skip_type):
        def process(msg, file_path):
            match = re_obj.search(msg)
            if match:
                logger.info(template, path.basename(file_path), match.group(1))
                msg_post_proc(file_path, ProcessedStatus.SKIPPED)
                counters["skipped_detail"][
                    f"{skip_type}: {match.group(1).lower()}"] += 1
                return True
            return False
        return process

    procs = []
    if skip_senders:
        re_part = \
            fr"(?:{'|'.join([re.escape(sender) for sender in skip_senders])})"
        re_senders = re.compile(fr"^from:.+<({re_part})>", re.I | re.M)
        procs.append(
            create_process(re_senders, "skipped %s from <%s>", "from"))
    if skip_subjects:
        re_part = \
            fr"(?:{'|'.join([re.escape(subj) for subj in skip_subjects])})"
        re_subjects = re.compile(fr"^subject:.+({re_part})", re.I | re.M)
        procs.append(create_process(
            re_subjects, 'skipped %s with subject containing "%s"', "subject"))

    def process_skip(msg, file_path):
        for proc in procs:
            if proc(msg, file_path):
                return True
        return False

    return process_skip


def create_msg_from_processor(mangle_domains, counters):
    """Mangle from header for certain domains"""
    def noop_fn(msg):
        return msg

    if not mangle_domains:
        return noop_fn

    re_part = fr"(?:{'|'.join([re.escape(dom) for dom in mangle_domains])})"
    re_domains = re.compile(fr"(from:.+<.+@.*{re_part})>", re.I | re.M)

    def process_from(msg):
        new_msg, num_subs = re_domains.subn(r"\1_>", msg, 1)
        counters["mangled"] += num_subs
        return new_msg

    return process_from


def msg_generator(config, msg_post_proc, counters):
    """Generate a message from each EML file in a directory"""
    pattern = path.join(config["eml_dir"], "*.eml")
    re_newline = re.compile(r"\r(?!\n)|(?<!\r)\n")

    process_skip = create_msg_skip_processor(config["skip_senders"],
                                             config["skip_subjects"],
                                             msg_post_proc, counters)
    process_from = create_msg_from_processor(config["mangle_domains"],
                                             counters)

    logger.info("processing EML dir: %s", config["eml_dir"])
    for i, file_path in enumerate(glob.glob(pattern)):
        if i - counters["skipped"] >= config["max_msgs"]:
            break
        with open(file_path, "r", encoding="utf-8") as eml_file:
            msg = eml_file.read()
        if process_skip(msg, file_path):
            continue
        yield (file_path, re_newline.sub("\r\n", process_from(msg)))


def send_emls(config, msg_gen, msg_post_proc):
    """Send EMLs in a directory to an SMTP server"""
    logger.info("connecting to %s:%s",
                config["smtp_host"], config["smtp_port"])
    try:
        tick = perf_counter()
        backoff_tick = tick
        with smtplib.SMTP(config["smtp_host"], config["smtp_port"]) as client:
            for file_name, msg in msg_gen:
                tock = perf_counter()
                # Stop sending messages if max_time elapsed
                if tock - tick >= config["max_time"]:
                    break
                # Pause sending messages if backoff_interval elapsed
                if tock - backoff_tick >= config["backoff_interval"]:
                    logger.info("backing off")
                    # If the backoff_duration is long, quit then reconnect to
                    # the server once the duration has elapsed
                    if config["backoff_duration"] > 5:
                        client.quit()
                        sleep(config["backoff_duration"])
                        logger.info("reconnecting to %s:%s",
                                    config["smtp_host"], config["smtp_port"])
                        client.connect(config["smtp_host"],
                                       config["smtp_port"])
                    else:
                        sleep(config["backoff_duration"])
                    backoff_tick = perf_counter()
                logger.info("sending %s", path.basename(file_name))
                # Attempt to send message. Intercept any exceptions we don't
                # want the context manager to handle here, so we can keep the
                # client open and stay in the loop.
                try:
                    processed_status = ProcessedStatus.UNPROCESSED
                    client.sendmail(config["from_addr"], config["to_addr"],
                                    msg.encode("utf-8"))
                    processed_status = ProcessedStatus.PROCESSED
                # Reconnect and continue sending messages on data error
                except (smtplib.SMTPDataError) as exc:
                    logger.error(exc, exc_info=exc)
                    client.quit()
                    logger.info("reconnecting to %s:%s",
                                config["smtp_host"], config["smtp_port"])
                    client.connect(config["smtp_host"], config["smtp_port"])
                # Route message to processed/unprocessed dir
                finally:
                    msg_post_proc(file_name, processed_status)
    finally:
        tock = perf_counter()
        logger.info2("sent messages for %.2f seconds", tock - tick)


def summarize(counters):
    """Log a summary of the counters"""
    def reduce_dict(stats, indent, width, initial=""):
        return reduce(
            lambda x, y:
                f"{x}\n{'':>{indent}}{y[:width - 3]:.<{width}} {stats[y]}"
            if not isinstance(stats[y], dict)
            else reduce_dict(stats[y], indent + 4, 30, x),
            stats, initial)

    if not counters:
        return
    total_counters = {"TOTAL": counters["processed"] + counters["unprocessed"]
                      + counters["skipped"], **counters}
    logger.info2(reduce_dict(total_counters, 29, 15, "Statistics"))


def main():
    """Run the application"""
    try:
        exit_code = 0

        logger.info2("%s started", sys.argv[0])

        ini_file = get_args()
        logger.info("args: %s", ini_file)
        config = reduce(lambda x, y: {**x, **y}, get_config(ini_file).values())
        counters = {"processed": 0, "unprocessed": 0, "skipped": 0,
                    "skipped_detail": defaultdict(int), "mangled": 0}
        msg_post_proc = create_msg_post_processor(**config, counters=counters)
        msg_gen = msg_generator(config, msg_post_proc, counters)
        send_emls(config, msg_gen, msg_post_proc)

    except Exception as exc:  # pylint: disable=broad-except
        logger.critical(exc, exc_info=exc)
        exit_code = 1

    finally:
        summarize(counters)
        logger.info2("%s finished", sys.argv[0])
        sys.exit(exit_code)


if __name__ == "__main__":
    logger = create_logger()
    main()
